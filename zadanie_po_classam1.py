from typing import List


class Pupil:
    def __init__(self, name: str):
        self.memory = []
        self.name = name

    def learn(self, a: str) -> None:
        self.memory.append(a)

    def share_memory(self) -> List[str]:
        print(self.name)
        print(self.memory)
        return self.memory


a: Pupil = Pupil("Vanya")  #
a.learn('I am Groot')  #
# a.share_memory()  #
b: Pupil = Pupil("Fedya")
b.learn('I am pupil')


# b.share_memory()

# собака - дочерний класс, пьюпил - родительский
class Sobaka(Pupil):
    def share_memory(self) -> List[str]:
        print('Bark')
        return ['Bark']

    def jump(self) -> None:
        print('jump')


dog: Sobaka = Sobaka("Barbara")
dog.learn('Good girl')
# dog.share_memory()
# dog.jump()
pupils: List[Pupil] = [a, b, dog]


# for i in range(len(pupils)):
#     pupils[i].share_memory()

class Teacher:
    def __init__(self):
        self.pupils: List[Pupil] = []

    def add_one(self, pupil: Pupil) -> None:
        self.pupils.append(pupil)

    def teach(self, inp: str) -> None:
        for i in range(len(self.pupils)):
            self.pupils[i].learn(inp)

    def ask(self) -> None:
        for i in range(len(self.pupils)):
            self.pupils[i].share_memory()


# teacher: Teacher = Teacher()
# teacher.add_one(a)
# teacher.teach('ты говно')
# teacher.ask()
# teacher.add_one(dog)
# teacher.teach('1+1=2')
# teacher.ask()
class Schoolboy(Pupil):
    def share_memory(self) -> List[str]:
        n: int = len(self.memory) - 1
        print(self.memory[n])
        return [self.memory[n]]


teacher: Teacher = Teacher()
bob: Schoolboy = Schoolboy("Bob")
teacher.add_one(dog)
teacher.add_one(bob)
teacher.teach("Валерий это мужское имя")
teacher.teach("Валерия это женское имя")
teacher.ask()


class ToxicTeacher(Teacher):
    def __init__(self):
        super().__init__()
        self.information_count = 0

    def teach(self, inp) -> None:
        super().teach(inp)
        self.information_count += 1

    def ask(self) -> None:
        for i in range(len(self.pupils)):
            b: List[str] = self.pupils[i].share_memory()
            if self.information_count > len(b):
                print(self.pupils[i].name, 'is Durak!')
            else:
                print(self.pupils[i].name, 'is Molodec')

class SmartDog(Sobaka):
    def share_memory(self) -> List[str]:
        n = len(self.memory)
        print(["Smart_Bark"] * n)
        return ['Smart_Bark'] * n


toxic_teacher: ToxicTeacher = ToxicTeacher()
pupil1: Pupil = Pupil("Oleg")
dog: Sobaka = Sobaka("Galya")
shkolnik: Schoolboy = Schoolboy("Leonid")
smart_dog: SmartDog = SmartDog("SmartDog")
toxic_teacher.add_one(pupil1)
toxic_teacher.add_one(dog)
toxic_teacher.add_one(shkolnik)
toxic_teacher.add_one(smart_dog)
toxic_teacher.teach("Жи ши пиши через и")
print("______________")
toxic_teacher.ask()
print("______________")
toxic_teacher.teach("Ча ща пиши через а")
toxic_teacher.ask()


def fibo(n):
    fibo1=1
    fibo2=1
    print(fibo1)
    print(fibo2)
    if n == 1:
        return 1
    if n == 2:
        return 1
    for i in range(n):
        fibo1,fibo2 = fibo2,fibo1+fibo2
        print(fibo2)

for i in range(10):
    print(fibo(i))
# def fibo_2(n):
#