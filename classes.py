# Наследование (наследование, инкапсуляция и полиморфизм)
from typing import List

# ООП - объектно ориентированное программирование
a: int = int("1")
b: str = "asd"
c: list = []


class Phone:
    def __init__(self, number: str, color: str):
        self.number = number
        self.color = color


class User:  # Класс
    def __init__(self, name: str, password: str, phone: Phone):  # конструктор класса
        self.name = name
        self.password = password  # параметры
        self.phone = phone

    def pretty_print(self, prefix: str):  # метод класса работает с экземпляром
        print(prefix, self.name, self.phone.color, self.phone.number)

    def check_password(self, password: str) -> bool:
        return password == self.password

    @staticmethod
    def govno():  # статический метод работает с классом
        print("govno")

    @classmethod
    def govno_1(cls) -> 'User':  # метод класса
        return cls("a", "s", Phone("a", "a"))


User.govno()
User.govno_1()


pidor_phone: Phone = Phone("+7900000000", "red")
pidor: User = User("pidor", "admin_password", pidor_phone)  # экземпляр класса, в скобках пишется что передается в конструктор

pidor.pretty_print("I AM")
print("IS PASSWORD CORRECT:", pidor.check_password("peees"))

# User - это класс
# pidor - экземпляр класса
# User("admin", "admin_password") - инициализация экземпляра класса User

# print(type(pidor))
# print(pidor.name)  # поля. параментры
# print(pidor.password)
# print(pidor.phone.number)
class Animal:  # базоый класс
    def __init__(self, name_of_animal: str):
        self.name_of_animal = name_of_animal

    def roar(self):
        print('RRRRRRRR', self.name_of_animal)


class Cat(Animal):
    def __init__(self, name_of_animal: str, name: str):
        super().__init__(name_of_animal)
        self.name = name

    def roar(self):
        super().roar()
        print('MEAYYYY', self.name)


class Dog(Animal):
    def roar(self):  # override переопределение
        print('GAV GAV bark bark')


class Duck(Animal):
    def swim(self):
        print("KKKKKKKKKKK")


cat: Cat = Cat("cat", "BARSIK")
dog: Dog = Dog("cat")
duck: Duck = Duck("DGA")

print(str(cat))

animals: List[Animal] = [cat, dog, duck]

for item in animals:
    item.roar()
