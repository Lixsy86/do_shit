import random
from datetime import datetime

mas1 = ['lol', 'kekek', 'cheburek']
mas2 = ['INFO', 'ERROR', 'GONVO', 'WARNING']
mas3 = ['/haha/lala.py', '/haha/lala/gaga.py']
mas4 = ['b', 'c', 'd', 'e', 'f', 'g']


def random_str() -> str:
    length = random.randint(3, 30)
    c = ''
    for _ in range(length):
        c += random.choice(mas4)
    return c


print(random_str())

with open('xui.txt', 'w') as f:
    for i in range(1000):
        foo = ['a', 'b', 'c', 'd', 'e']
        f.write(
            f"{datetime.now()} -- {random.choice(mas2)} :: {random.choice(mas1)} {random.choice(mas3)} {random_str()}: Success in: {random.random() * 10}s {random_str()} ")
        f.write('\n')


res = {
    '1': 20,
    '2': 10,
    '3': 8
}