import requests

r = requests.get("https://ru.wikipedia.org/wiki/%D0%90%D1%83%D1%82%D0%B8%D0%B7%D0%BC")
print(r)
print(r.content)

data: str = r.content.decode()
start = 0
end = len(data)
count = 0
end_bracket = '>'
end_bracket1 = '/'
for i in range(len(data)):
    s = data.find('<', start, end)
    if i > 0:
        if end_bracket or end_bracket1 not in range(s, s + 7):
            count = count + 1
    start = s + 1
print(count)
