import json
from typing import List, Any


def list_to_dict(a: List[Any]):
    list_dict = []
    for i in range(len(a)):
        list_dict.append(a[i].to_dict())
    return list_dict


class Zhivotnoe:
    def __init__(self, name: str, paws: int):
        self.name = name
        self.paws = paws

    def __repr__(self):
        return f"Zhivotnoe ({self.name}, {self.paws})"

    def to_dict(self) -> dict:
        return {"name": self.name,
                "paws": self.paws}

    def to_json(self) -> str:
        return json.dumps(self.to_dict())

    @staticmethod
    def from_json(data: str) -> "Zhivotnoe":
        dict_data: dict = json.loads(data)
        return Zhivotnoe(dict_data["name"], dict_data["paws"])


class Owner:
    def __init__(self, name: str, animals: List[Zhivotnoe]):
        self.name = name
        self.animals = animals

    def mas_to_dict(self) -> List[dict]:
        list_dict = []
        for i in range(len(self.animals)):
            list_dict.append(self.animals[i].to_dict())
        return list_dict

    def to_dict(self) -> dict:
        return {"name": self.name,
                "animals": self.mas_to_dict()}

    def to_json(self):
        return json.dumps(self.to_dict())


mouse: Zhivotnoe = Zhivotnoe("Valera", 4)
mas_animals = []
mas_animals.append(mouse)
owner: Owner = Owner("Katya", mas_animals)
print(owner.to_json())


class DataBase:
    def __init__(self):
        self.owners: List[Owner] = []
        self.animals: List[Zhivotnoe] = []

    def add_owner(self, owner: Owner):
        self.owners.append(owner)
        self.save_to_file()

    def add_animal(self, animal: Zhivotnoe):
        self.animals.append(animal)
        self.save_to_file()

    def save_to_file(self):
        with open("database_animals.json", "w") as f:
            dict_data = list_to_dict(self.animals)
            f.write(json.dumps(dict_data))

        with open("database_owners.json", "w") as f:
            dict_data = list_to_dict(self.owners)
            f.write(json.dumps(dict_data))

    def load_from_file(self):
        with open ("database_animals.json","r") as f:
            a = f.read()
        

database: DataBase = DataBase()
database.add_animal(mouse)
database.add_owner(owner)

def find_indexes_before_and_after_float(float_find_by_first_and_last_indexes: str) -> (int, int):
    for i in range(len(float_find_by_first_and_last_indexes)):
        if float_find_by_first_and_last_indexes[i] == "i" and float_find_by_first_and_last_indexes[i + 1] == "n" and \
                float_find_by_first_and_last_indexes[i + 2] == ":":
            first_index = i + 5
            print(first_index)
        if float_find_by_first_and_last_indexes[i] == "R" and float_find_by_first_and_last_indexes[i + 1] == "e" and \
                float_find_by_first_and_last_indexes[i + 2] == "s":
            last_index = i - 2
            print(last_index)
    return first_index, last_index


# need_to_func = find_indexes_before_and_after_float(find_by_space_before_words)


def find_float(need_to_func: (int, int)) -> List[str]:
    need_to_func = find_indexes_before_and_after_float(find_by_space_before_words)
    result = []
    for i in range(len(need_to_func)):
        result.append(str(need_to_func[i]))
        print(result)
    return result