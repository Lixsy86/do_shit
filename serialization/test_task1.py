from tasks import Sumator


#
# assert s.add_many(1,2) == [11]
# assert s.add_many(1,2, 3, 4 ,5 ,6, 1) == [6, 15]
# assert s.add_many(2) == []
# assert s.add_one(3) ==  6
s: Sumator = Sumator(3)
assert s.add_one(1) is None
assert s.add_one(2) is None
assert s.add_one(3) == 6
assert s.add_one(2) is None
assert s.add_one(5) is None
assert s.add_one(7) == 14
assert s.add_one(8) is None
assert s.add_many(1,2) == [11]
assert s.add_many(1, 2, 3, 4 ,5 ,6, 1) == [6, 15]
assert s.add_many(2) == []
assert s.add_one(3) == 6
