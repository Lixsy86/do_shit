import json


class Zhivotnoe:
    def __init__(self, name: str, paws: int):
        self.name = name
        self.paws = paws

    def __repr__(self):
        return f"Zhivotnoe ({self.name}, {self.paws})"

    def to_dict(self) -> dict:
        return {"name": self.name,
                "paws": self.paws}

    def to_json(self) -> str:
        return json.dumps(self.to_dict())

    @staticmethod
    def from_json(data: str) -> "Zhivotnoe":
        dict_data:dict = json.loads(data)
        return Zhivotnoe(dict_data["name"],dict_data["paws"])


sobaka: Zhivotnoe = Zhivotnoe("gavka", 4)
print(sobaka.to_json())


def save_to_file():
    sobaka: Zhivotnoe = Zhivotnoe("gavka", 4)
    sobaka: str = sobaka.to_json()
    with open("gavka.txt", "w") as f:
        f.write(sobaka)


save_to_file()


def load_from_json():
    with open("gavka.txt", "r") as f:
        data:str  = f.read()
        gavka: Zhivotnoe = Zhivotnoe.from_json(data)  #### Только тут добавляем staticmethod
        print(gavka)


load_from_json()