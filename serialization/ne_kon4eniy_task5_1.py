import json
from typing import List, Set


def find_id_with_parents(people: List[dict]) -> Set[int]:
    ids = set()
    for person in people:
        if person['age'] > 14 and 'child_ids' in person:
            for child in person['child_ids']:
                ids.add(child)
    return ids


def main() -> List[dict]:
    with open('data.json', 'rb') as f:
        data = f.read().decode()
        children = json.loads(data)['children']
        with_parents = find_id_with_parents(children)
        answer = []
        for person in children:
            if person['age'] < 14 and person['id'] not in with_parents:
                answer.append({
                    'id': person['id'],
                    'age': person['age'],
                    'name': person['name'],
                })
        return answer


print(main())
