# with open('lalka.txt', 'w') as f:  ### (create, read,write, append)
#    f.write('privet')


# with open('lalka.txt', 'r') as f:
#    data = f.read()
#    print(data)

import os
import json  # работает только со словарями

try:
    os.mkdir('tmp')  # создать папку
except:
    print('pobat')

with open('tmp/lalka.txt', 'w') as f:
    f.write('poka')
    f.write('hello')


# Сериализация

class User:
    def __init__(self, name, code):
        self.name = name
        self.code = code

    def to_dict(self) -> dict:
        return {
            "name": self.name,
            "code": self.code
        }

    def to_json(self) -> str:
        return json.dumps(self.to_dict())

    @staticmethod
    def from_json(data: str) -> 'User':
        dict_data: dict = json.loads(data)
        return User(dict_data['name'], dict_data['code'])

    def __repr__(self) -> str:
        return "User(name=" + self.name + ", code=" + str(self.code) + ")"


def save_user_to_file():
    user: User = User('Ivan', 10)

    egor: str = user.to_json()

    with open('user1.json', 'w') as f:
        f.write(egor)


def load_user():
    with open('user1.json', 'r') as f:
        data: str = f.read()
        user: User = User.from_json(data)
        print(user)


load_user()


def govno():
    dict_data = {
        "name": "ivan",
        "code": 10,
        "obj": {
            'a': "b"
        },
        "list": [1, 2],
        "asd": [{}, {}]
    }

    ser_data = json.dumps(dict_data)  # метод dumps превращает словарь в строку

    with open('heh.json', 'w') as f:
        f.write(ser_data)

    with open('heh.json', 'r') as f:
        data = f.read()
        print(json.loads(data))  # json =библиотека питона/ формат передачи данных load = load, s = string



