import random
from typing import List, Union, Optional, Any

# mapa = [
#     [False, False, False],
#     [False, False, False],
#     [False, False, False],
# ]
ACTUAL_MAP = []
BOMB_MAP = []


#  def map(actual_map):
# #     a = []
# #     b='-'
# #     for i in range(len(actual_map)):
# #         a.append(b)
# #     return a
def map(actual_map: List[List[Union[str, int]]]) -> None:
    for i in range(len(actual_map)):
        for j in range(len(actual_map[i])):
            print(actual_map[i][j], end='  ')
        print()


# def map(k, m):
#     actual_map=[]
#     s1='-'
#     for k in range(len(actual_map)):
#         actual_map.append(s1)
#         for m in range(len(actual_map[i])):
#             actual_map.append(s1)
#             print(actual_map[k][m],end = '  ')
#         print()

def one_stroka(k: int) -> List[str]:
    mas: List[str] = []
    for i in range(k):
        mas.append('-')
    return mas


def generate_map(k: int, m: int) -> List[List[str]]:
    mas: List[List[str]] = []
    for i in range(m):
        mas.append(one_stroka(k))
    return mas


def two_stroka(k: int) -> List[bool]:
    mas: List[bool] = []
    for i in range(k):
        mas.append(False)
    return mas


def generate_bomb_map(k: int, m: int) -> List[List[bool]]:
    bomb_map: List[List[bool]] = []
    for i in range(m):
        bomb_map.append(two_stroka(k))
    return bomb_map


def get_random_indexes(matrix: List[List[Any]]) -> (int,int):
    random_i: int = random.randint(0, len(matrix) - 1)
    random_j: int = random.randint(0, len(matrix[random_i]) - 1)
    print(random_i, random_j)
    return random_i, random_j


def add_one_random_bomb(matrix: List[List[bool]]) -> None:
    random_i, random_j = get_random_indexes(matrix)
    matrix[random_i][random_j] = True


def chislo_input(data: str) -> (int,int):
    data: List[str] = data.split(',')
    return int(data[0]), int(data[1])


### НАПИСАТЬ СЕБЕ ЗАДАНИЕ
# Создать две мапы используя chislo_input. Указать, что i,j это задаваемые величины от игрока к обоим мапам
# 1.Написать функцию ввода параметров (длина,ширина), кол-ва бомб от лица игрока
# Проверить, чтобы сгенерированные бомбы были под разными индексами
# 2.вывести эту функцию в консоль
# Напишем функцию, которая добавляет одну бомбу
# функцию, добавляет n рандомных бомб
# починить дублирование бомб
def add_n_random_bomb(bomb_map: List[List[bool]], n: int) -> None:
    while True:
        random_i, random_j = get_random_indexes(bomb_map)
        bomb_map[random_i][random_j] = True
        sum_bomb = 0
        sum_n = n
        for i in range(len(bomb_map)):
            for j in range(len(bomb_map[i])):
                if bomb_map[i][j]:
                    sum_bomb = sum_bomb + 1
        if sum_n == sum_bomb:
            return

    # c = 0
    # for i in range(len(matrix)):
    #     for j in range(len(matrix[i])):
    #         if matrix[i][j] == add_one_random_bomb(matrix):
    #             c = c+1
    #     else:
    #          add_one_random_bomb(matrix)
    #     if c == n:
    #         return matrix

# while True:
#     s1 = False
#     data = input()
#     k,m = chislo_input(data)
#     # a = [['-' for i in range(k)] for j in range(m)]
#     map(generate_map(k,m))
