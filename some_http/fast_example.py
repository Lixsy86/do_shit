import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class PowelTi(BaseModel):
    a: int
    b: int


@app.post("/sum_a_and_b/")
def read_root1(item: PowelTi, a: int):
    summa = item.a + item.b
    return summa * a


@app.get("/prevet/medved")
def read_root():
    print('ebola')
    return 'aaaa'


@app.get("/sum_a_and_b/")
def read_root1(a: int, b: int):
    return a + b


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8040)
