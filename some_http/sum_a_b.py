import requests

resp = requests.get(
    url='http://127.0.0.1:8040/sum_a_and_b?a=1&b=10',
    headers={'Content-Type': 'pidar'},
)

print('code', resp.status_code)
print('payload', resp.content)
print(resp.headers)
print()