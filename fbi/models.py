from typing import List


class NotFoundException(Exception):
    pass


class User:
    def __init__(self, name: str, passport: str):
        self.name = name
        self.passport = passport

    def __repr__(self):
        return "User(" + self.name + ' : ' + self.passport + ")"


class Car:
    def __init__(self, user: User, number: str):
        self.user = user
        self.number = number

    def __repr__(self):
        return "Car(" + self.user.passport + ' : ' + self.number + ")"


class TrafficFine:
    def __init__(self, user: User, car: Car, is_paid: bool = False):
        self.user = user
        self.car = car
        self.is_paid = is_paid

    def __repr__(self):
        return "TrafficFine(" + self.user.passport + " : " + self.car.number + ")"


class DataBase:
    def __init__(self):
        self.users: List[User] = []
        self.cars: List[Car] = []
        self.fines: List[TrafficFine] = []

    def add_user(self, user: User):
        self.users.append(user)

    def add_users(self, *args: User) -> None:
        for i in range(len(args)):
            self.users.append(args[i])

    def add_car(self, car: Car):
        self.cars.append(car)

    def add_fine(self, fine: TrafficFine):
        self.fines.append(fine)

    def fine_by_passport_and_car_number(self, passport: str, number: str):
        user1: User = self.find_user_by_passport(passport)
        users_car: Car = self.find_car_by_car_number(number)
        create_fine: TrafficFine = TrafficFine(user1, users_car)
        self.add_fine(create_fine)

    def find_user_by_passport(self, passport: str) -> User:
        for i in range(len(self.users)):
            if passport == self.users[i].passport:
                return self.users[i]
        raise NotFoundException("User not found")

    def find_car_by_car_number(self, car_number: str) -> Car:
        for i in range(len(self.cars)):
            if car_number == self.cars[i].number:
                return self.cars[i]
        raise NotFoundException("Car not found")

    def find_fines_by_passport(self, passport: str) -> List[TrafficFine]:
        mas_fines = []
        for i in range(len(self.fines)):
            if passport == self.fines[i].user.passport and not self.fines[i].is_paid:
                mas_fines.append(self.fines[i])
        return mas_fines

    def pay_fine_by_passport_and_car_number(self, passport: str, car_number):
        for i in range(len(self.fines)):
            if self.fines[i].user.passport == passport and self.fines[i].car.number == car_number:
                self.fines[i].is_paid = True
