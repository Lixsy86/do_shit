from typing import List

class HasGone(Exception):
    pass

class Stock:
    pass


class Product:
    def __init__(self, name: str, quantity: str):
        self.name = name
        self.quantity = quantity

    def __repr__(self):
        return self.name + " : " + self.quantity


class Store:
    def __init__(self, adress: str):
        self.adress = adress


class Sixterochka:
    def __init__(self):
        self.adresses: List[Store] = []
        self.position: List = []
        self.position_quantity: List[List] = []
        self.stock: List[Product] = []

    def add_adress(self, adress: Store):
        self.adresses.append(adress)

    def add_product_to_store(self, product: Product):
        self.stock.append(product)

    def check_position(self, name: str):
        for i in range(len(self.position)):
            if name == self.position[i].name:
                return name
        raise HasGone(" This product got sold out ")

    def check_quantity(self,quantity:str):
        for i in range(len(self.position_quantity)):
            if self.position_quantity[i] > 0 and quantity <= self.position_quantity[i]:
                return quantity

    # def buy_product_from_store(self, stock: List[Product], name: Product, quantity: str):
    #     for i in range(len(stock)):
    #         if stock[i].quantity == 0:
    #             del stock[i]

    dict1 = {"name:Product" : "moloko",
             "kolvo" : "4islo"}
