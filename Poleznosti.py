# Java Kotlin C# C++ - языки со статической типизацией

# int bubbleSort(int[] arr) {
#     int n = arr.length;
#     int temp = 0;
#     for (int i=0; i < n; i++)
#     {
#         for (int j=1; j < (n-i); j++){
#             if (arr[j-1] > arr[j]){
#                 temp = arr[j-1];
#                 arr[j-1] = arr[j];
#                 arr[j] = temp;
#             }
#
#     }
#      return 1;
# }

# Python JavaScript R Ruby - языки с динамической типизацией

# Type hinting Python
from typing import List, Union, Optional


def bubble_sort(mas: List[int]) -> None:
    length: int = len(mas)
    for i in range(length):
        for j in range(length - i - 1):
            mas[j], mas[j + 1] = mas[j + 1], mas[j]


def big_dig_func(
        str_or_int: Union[str, int],
        number: int,
        string: str,
        flag: bool,
        floater: float,
        int_mas: List[int],
        str_mas: List[str],
        bool_mas: List[bool],
        bomp_map: List[List[bool]],
        actual_map: List[List[Union[str, int]]],
        optional_int: Optional[int]) -> None:  # int но может быть и None
    pass


def find_number_gte_10(mas: List[int]) -> Optional[int]:
    for i in range(len(mas)):
        if mas[i] > 10:
            return mas[i]
    return None


def generate_bomp_map(h: int, w: int) -> List[List[bool]]:
    pass


result: Optional[int] = find_number_gte_10([1, 2, 3, 4, 5])


# ctrl + Q  посмотреть СИГНАТУРУ функции ctrl + B зайти в функцию
# alt + Enter HEEEEELP

def check(a: int) -> bool:
    return a > 10


print(result)
