# Вывести карту актуал +
# Ввести функцию обработки ввода от игрока +
# каждый ход выводить карту заново +
# Если в индексах бомба: отображать бомбы на актуальной карте написать "ТЫ ЛОХ" +
# если бомбы нет: посчитать кол-во бомб смежных с выбранным индексом и отобразить число +

# сделать функцию Победы и проверять после каждого хода ?
from generator_matric import generate_bomb_map, add_n_random_bomb, generate_map
from typing import List, Union, Optional


# BOMB_MAP = [[False, False, False, False, False, True],
#             [False, False, False, False, False, False],
#             [False, False, False, False, False, False],
#             [False, False, False, False, False, False],
#             [False, False, False, False, False, False]
#             ]
# ACTUAL_MAP = [['-', '-', '-', '-', '-', '-'],
#               ['-', '-', '-', '-', '-', '-'],
#               ['-', '-', '-', '-', '-', '-'],
#               ['-', '-', '-', '-', '-', '-'],
#               ['-', '-', '-', '-', '-', '-']
#               ]


def map(actual_map: List[List[Union[str, int]]]) -> None:
    for i in range(len(actual_map)):
        for j in range(len(actual_map[i])):
            print(actual_map[i][j], end='  ')
        print()


def chisla_input(chisla:str) -> (int,int):
    chisla = chisla.split(',')
    return int(chisla[0]), int(chisla[1])


def showbombs(actual_map: List[List[Union[str, int]]], bomb_map: List[List[bool]]) -> None:
    for i in range(len(bomb_map)):
        for j in range(len(bomb_map[i])):
            if bomb_map[i][j] == True:
                actual_map[i][j] = '*'


def bombquantity(bomb_map: List[List[bool]], i: int, j: int) -> int:
    bomb_sum = 0
    if j + 1 < len(bomb_map[i]) and bomb_map[i][j + 1]:
        bomb_sum = bomb_sum + 1
    if j - 1 >= 0 and bomb_map[i][j - 1]:
        bomb_sum = bomb_sum + 1
    if i - 1 >= 0 and bomb_map[i - 1][j]:
        bomb_sum = bomb_sum + 1
    if i - 1 >= 0 and j + 1 < len(bomb_map[i]) and bomb_map[i - 1][j + 1]:
        bomb_sum = bomb_sum + 1
    if i - 1 >= 0 and j - 1 >= 0 and bomb_map[i - 1][j - 1]:
        bomb_sum = bomb_sum + 1
    if i + 1 < len(bomb_map) and bomb_map[i + 1][j]:
        bomb_sum = bomb_sum + 1
    if i + 1 < len(bomb_map) and j + 1 < len(bomb_map[i]) and bomb_map[i + 1][j + 1]:
        bomb_sum = bomb_sum + 1
    if i + 1 < len(bomb_map) and j - 1 >= 0 and bomb_map[i + 1][j - 1]:
        bomb_sum = bomb_sum + 1
    return bomb_sum


def vokrug(actual_map: List[List[Union[str, int]]], bomb_map: List[List[bool]], i: int, j: int) -> None:  ####!
    if j + 1 < len(bomb_map[i]) and actual_map[i][j + 1] == '-':
        actual_map[i][j + 1] = bombquantity(bomb_map, i, j + 1) or '+'
        if actual_map[i][j + 1] == '+':
            vokrug(actual_map, bomb_map, i, j + 1)
    if j - 1 >= 0 and actual_map[i][j - 1] == '-':
        actual_map[i][j - 1] = bombquantity(bomb_map, i, j - 1) or '+'
        if actual_map[i][j - 1] == '+':
            vokrug(actual_map, bomb_map, i, j - 1)
    if i + 1 < len(bomb_map) and actual_map[i + 1][j] == '-':
        actual_map[i + 1][j] = bombquantity(bomb_map, i + 1, j) or '+'
        if actual_map[i + 1][j] == '+':
            vokrug(actual_map, bomb_map, i + 1, j)
    if i - 1 >= 0 and actual_map[i - 1][j] == '-':
        actual_map[i - 1][j] = bombquantity(bomb_map, i - 1, j) or '+'
        if actual_map[i - 1][j] == '+':
            vokrug(actual_map, bomb_map, i - 1, j)
    if j + 1 < len(bomb_map[i]) and i + 1 < len(bomb_map) and actual_map[i + 1][j + 1] == '-':
        actual_map[i + 1][j + 1] = bombquantity(bomb_map, i + 1, j + 1) or '+'
        if actual_map[i + 1][j + 1] == '+':
            vokrug(actual_map, bomb_map, i + 1, j + 1)
    if j - 1 >= 0 and i - 1 >= 0 and actual_map[i - 1][j - 1] == '-':
        actual_map[i - 1][j - 1] = bombquantity(bomb_map, i - 1, j - 1) or '+'
        if actual_map[i - 1][j - 1] == '+':
            vokrug(actual_map, bomb_map, i - 1, j - 1)
    if j - 1 >= 0 and i + 1 < len(bomb_map) and actual_map[i + 1][j - 1] == '-':
        actual_map[i + 1][j - 1] = bombquantity(bomb_map, i + 1, j - 1) or '+'
        if actual_map[i + 1][j - 1] == '+':
            vokrug(actual_map, bomb_map, i + 1, j - 1)
    if j + 1 < len(bomb_map[i]) and i - 1 >= 0 and actual_map[i - 1][j + 1] == '-':
        actual_map[i - 1][j + 1] = bombquantity(bomb_map, i - 1, j + 1) or '+'
        if actual_map[i - 1][j + 1] == '+':
            vokrug(actual_map, bomb_map, i - 1, j + 1)


def winner(actual_map: List[List[Union[str, int]]], bomb_map: List[List[bool]]) -> bool:
    sum_bomb = 0
    sum_palk = 0
    for i in range(len(bomb_map)):
        for j in range(len(bomb_map[i])):
            if bomb_map[i][j]:
                sum_bomb = sum_bomb + 1
    for i in range(len(actual_map)):
        for j in range(len(actual_map[i])):
            if actual_map[i][j] == '-':
                sum_palk = sum_palk + 1
    if sum_palk == sum_bomb:
        return True
    if sum_palk != sum_bomb:
        return False


def mainfunc(actual_map: List[List[Union[str, int]]], bomb_map: List[List[bool]], i: int, j: int) -> bool:
    if bomb_map[i][j]:
        showbombs(actual_map, bomb_map)
        print('Loser')
        return True
    b = bombquantity(bomb_map, i, j)
    if b == 0:
        actual_map[i][j] = '+'
        vokrug(actual_map, bomb_map, i, j)
    if b > 0:
        actual_map[i][j] = b
    if winner(actual_map, bomb_map):
        print("Хорошая работа")
        showbombs(actual_map, bomb_map)
        return True


print('Пожалуйста, введите размер карты')
chisla = input()
k, m = chisla_input(chisla)
while k == 0 or m == 0:
    print('Введите достойное значение')
    chisla = input()
    k, m = chisla_input(chisla)
ACTUAL_MAP = generate_map(k, m)
print("Введите кол-во бомб")
chislo = input()
chislo = int(chislo)
n: int = chislo
while n == 0:
    print('Хотя бы одну')
    chislo = input()
    n = int(chislo)
BOMB_MAP = generate_bomb_map(k, m)
add_n_random_bomb(BOMB_MAP, n)
while True:
    map(ACTUAL_MAP)
    print('Куда стрелять?')
    chisla = input()
    k, m = chisla_input(chisla)
    if mainfunc(ACTUAL_MAP, BOMB_MAP, k, m):
        map(ACTUAL_MAP)
        break
