# Строки

i = 1
f = 0.1
b = False
s1 = "Privet Привет"
s2 = 'Privet Привет'

print(str(10) == "10")
print(s2)


print(s1[1], s1[0])

print(len(s1))

for i in range(len(s1)):
    print(s1[i])

print(s1 == s2)
print("При" in s1)
print(s1[2:10])
print([1,2,3,4,5][1:3])

print(s1.replace("Privet", "ДАРОВА"))
replaced_str = s1.replace("Privet", "ДАРОВА")
print(replaced_str)
print(s1)

words = s1.split()
print(words)
words = "Privet!!!Привет!!!asd".split("!!!")
print(words)
print(len(words))
