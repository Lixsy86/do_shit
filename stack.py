# 1. создаем пустой стек
# 2. берем все скобки по порядку
# 3. если скобка открывающая, то кладем её в стек.
# 4. если скобка закрывающая, то мы смотрим на верхнюю скобку стека.
#   и если они парные, то удаляем скобку из стека и идем дальше. Если разные, падаем.
# 5. когда мы обошли все скобки, смотрим на стек. если он пуст, то последовательность верна


# BRACKETS = {
#     '(': ')',
#     '[': ']',
#     '{': '}'
# }
#
#
# def check_brackets(seq: str) -> bool:
#     stack = []
#     for i in range(len(seq)):
#         if seq[i] in BRACKETS:  # есть ли seq[i] в ключах BRACKETS (то есть является ли открывающей)
#             stack.append(seq[i])
#         else:
#             if len(stack) == 0:
#                 return False
#             last_bracket = stack[len(stack) - 1]  # верхняя скобка в стеке
#             if BRACKETS[last_bracket] == seq[i]:
#                 stack.pop()
#             else:
#                 return False
#
#     return len(stack) == 0
#
#
# check_brackets('([])')




# def check_brackets(seq: str) -> bool:
#     j = 0
#     for i in range(len(seq)):
#         if j < 0:
#             return False
#         if seq[i] == "(":
#             j = j + 1
#         if seq[i] == ")":
#             j = j - 1
#     if j != 0:
#         return False
#     else:
#         return True
#
#
# print(check_brackets("(()(()))"))
