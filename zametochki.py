def max_neibours(mus):
    res = []
    if len(mus) == 1:
        return [mus[0]]

    for i in range(len(mus)):
        if i == 0:
            if mus[i] > mus[i + 1]:
                res.append(mus[i])
        elif i == len(mus) - 1:
            if mus[i] > mus[i - 1]:
                res.append(mus[i])
        else:
            if mus[i] > mus[i - 1] + mus[i + 1]:
                res.append(mus[i])
    return res

def max_unique_neighbours(mas):
    res_not_unique = max_neibours(mas)
    res_unique = set(res_not_unique)
    return res_unique


def max_sorted_neibours(mas):
    max_sorted = max_neibours(mas)
    max_sorted.sort()
    return max_sorted


def the_max_neibours(mas):
    res = max_neibours(mas)
    return max_neibours(res)


def the_most_max(mas):
    res_1 = the_max_neibours(mas)
    return the_max_neibours(res_1)


mas = ["", "*", "", "*", "*"]

matrix = [
    ["", "*", "", "*", "*"],
    ["", "*", "", "", "*"],
    ["*", "", "", "*", "*"]
]

# матрица это массив массивов
print(len(matrix))
print(matrix[0], len(matrix[0]))  # первая стчка
print(matrix[1], len(matrix[1]))
print(matrix[2], len(matrix[2]))

print(matrix[0][1])

# if mas[k] == "*":
#    print(None)
# if mas[k] != "*":
#    sum = 0
#    if mas[k-1] == "*":
#        sum = sum + 1
#    if mas[k+1] == "*":
#        sum = sum + 1

matrix = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
]


for i in range(len(matrix)):
    for j in range(len(matrix[i])):
        print(matrix[i][j], end='!')
    print()

    import random

    print("Привет")
    print("Угадай число или сдохни")

    good_number = random.randint(0, 10)
    bad_number = random.randint(0, 10)

    while True:
        your_number = input()
        if int(your_number) == good_number:
            print("Ты победил")
            break
        if int(your_number) == bad_number:
            print("Ты проиграл")
            break
